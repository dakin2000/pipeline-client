import json
import urllib2
from utils import post_damages


class Sensor():
	def __init__(self,pipe):
		self.activated = False
		self.damage_status = False
		self.pipe = pipe
		self.longitude = ''
		self.latitude = ''
		# self.data = {}

	def detect_holes(self):
		print 'when detecting holes ',self.pipe.damage_status
		if self.pipe.damage_status=='LEAKING':
			self.activated = True
			self.damage_status = 'LEAKING'
			self.damage_grade = 'SEVERE'
			# self.data["damage_count"] = self.pipe.damage_count
			# self.data['pipe_name'] = self.pipe.name
			# self.data["pipe_location"] = self.pipe.location
			return True
		else:
			return False

	def get_sensor_location(self):
		return (self.longitude, self.latitude)

	def get_pipe_location(self):
		return self.pipe.location

	def send_data(self):
		response = post_damages(self.pipe.slug,self.pipe.damage_status,self.pipe.damage_grade)
		return response
