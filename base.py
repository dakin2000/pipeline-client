from pipeline import Pipeline
from utils import damage_pipeline,all_pipelines
from sensor import Sensor
# getting information from vandals

pipelines = all_pipelines()
# print type(pipelines)
# print pipelines
results = pipelines['results']
output = '======================================================================\n'
output += 'ID Name       longitude       latitude       is_damaged     damage_grade\n'
output += '======================================================================\n'
# output += '{0}        {1}             {2}            {3}            {4}          \n'

print output
for i in results:
	print '{0}|   {1}|   {2}|   {3}|   {4}   {5}          \n'\
	.format(i['id'],i['name'],i['longitude'],i['latitude'],i['is_damaged'],i['damage_grade'])


pipe_id = raw_input("What is the ID of the pipeline you which to vandalize?\n")
pipeline = None
for pipe in results:
	if str(pipe['id']) == pipe_id:
		pipeline = pipe
		break


location = (pipe['longitude'],pipe['latitude'])
# location = raw_input("Please enter the location of the pipeline below (8733748,837287): \n")

# instantiating pipeline
# p = Pipeline(pipe_id,location)
p = Pipeline([pipe['name'],pipe['slug']],location)


# damaging the pipeline
status = raw_input("do you want to damage the pipeline?(Y/N)\n")
if status == 'Y':
	print "0 = None\n1 = Light\n2 = Severe\n3 = Devastating"
	pipe_damage_grade = int(raw_input("What is the level of damage, select from range 0-3?\n"))

	if pipe_damage_grade == 0:
		d = damage_pipeline(p,'LEAKING','NONE')
	elif pipe_damage_grade == 1:
		d = damage_pipeline(p,'LEAKING','LIGHT')
	elif pipe_damage_grade == 2:
		d = damage_pipeline(p,'LEAKING','SEVERE')
	elif pipe_damage_grade == 3:
		d = damage_pipeline(p,'LEAKING','DEVASTATING')
	else:
		print 'OK'

	# sensors detect damage
	s = Sensor(p)
	s_status = s.detect_holes()

	# checking if there is any damage
	if s_status:
		print "waiting for technical repair fromm server"
		s.send_data()
else:
	print "Thanks for the using the system."
