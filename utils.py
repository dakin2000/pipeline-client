import json
import requests

def damage_pipeline(pipe,status,grade):
	"""
	This function is responsible for making holes in the pipe.
	It accepts the pipe as an input.
	it return the damage status and level as output
	"""
	pipe.damage_count += 5
	pipe.damage_grade = grade
	pipe.damage_status = status
	# location = ''
	return pipe.damage_status


def all_pipelines():
	"""
	This function is responsible for Getting all the available
	pipelines and displaying it to the client..
	"""
	headers = {'Content-Type': 'application/json'}
	payload = {}
	# req = urllib2.Request('http://127.0.0.1:8000/api/pipelines/')
	# req.add_header('Content-Type', 'application/json')
	# response = urllib2.urlopen(req)
	r = requests.get('http://127.0.0.1:8000/api/pipeline/',headers=headers)
	# print dir(r)
	# print r.content
	return json.loads(r.content)

def post_damages(slug,is_damage,damage_grade):
	headers = {'Content-Type': 'application/json'}
	payload = {'is_damaged':is_damage,'damage_grade':damage_grade}
	data = json.dumps(payload)
	r = requests.put('http://127.0.0.1:8000/api/pipeline/{0}/update/'.format(slug),data=data,headers=headers)
	print slug,is_damage,damage_grade
	print r.content
	return json.loads(r.content)